# Quantum Computers in HPC #

This repository contains the final report for the course CS-E4600 - Advanced Topics in Software Systems.

## Introduction

Quantum computers (QCs) are machines that exploit the properties of quantum states, notably entanglement and superposition of particles, to perform computations. Compared to classical computers that output a well-defined result given a logical circuit, the output of a quantum circuit is probabilistic by nature.

The unit of information of QCs is a qubit, which represents the state of a quantum particle. Unlike the binary state of a classical bit, a qubit can be in a superposition, where it is both 0 and 1 at the same time until measured. The state of a qubit is often represented as a point on a Bloch sphere.

Several algorithms have been designed for QC that would exhibit extraordinary speedups (polynomial, exponential) over classical computers. Notable examples are Shor's algorithm for factorization, and Grover's algorithms for searching an unordered array. Quantum computer have also been proposed as a natural platform for performing quantum mechanics simulations and modeling other natural phenomena, such as protein folding.

Currently, it is believed that quantum computers will complement classical computers, not replace them. They excel in tasks where one needs to process an absurd amount of input permutations to output a single best answer.

However, quantum computing is still in the early research stage. It is not yet known whether practical quantum computers can ever be built. However, QCs offer an enormous potential and, if found practical, could be used to solve problems previously infeasible on classical computers. There are three major issues to overcome.

Firstly, noise. Qubits are exremely fragile and must be insulated from environmental interference by f.ex. cooling the system close to the absolute zero and shielding it from background radiation.

Secondly, scaling. For practical QCs, it is believed that millions of physical cubits need to be wired on a single chip. Implementing such complex wiring with the needed precision is not thought to be practical at the moment.

Thirdly, error correction. Classical error correction techniques do not work with quantum computers because of the no-cloning theorem. Currently, it is believed that to simulate one logical qubit, the system needs to have a thousand or more physical qubits. The experiments must also be run several times to produce a reliable output.



### Dependencies for the demo (Linux system) ###

* Python
* Jupyter notebook
* Qiskit (Available via f.ex. pip and Anaconda, see [the documentation](https://qiskit.org/documentation/getting_started.html) for more details)

### Running

* Start the jupyter notebook session (`> jupyter notebook`) and open the .ipynb file in the browser.


### Source code

The .ipynb notebook provides an example of possibly the simplest computation that is not possible with classical computers: true random number generation (in the sense that the random is not just a product of an extremely complex chaotic system, but rather a quantum mechanical phenomenom). The source code demonstrates the basic workflow for getting started with quantum computing, and shows how to submit jobs to actual quantum computers available on IBM Quantum.
